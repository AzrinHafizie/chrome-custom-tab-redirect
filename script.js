let link = 'https://www.cimb.com.my/en/personal/home.html'
let href = `<a href="${link}">Redirect with href</a>`
let body = ''

function redirectLocation(){
    window.location.replace(link)
}

function redirectHref(){
    window.location.href = link 

}

function redirectOpen() {
    window.open(link, "_self")
}

function init(){
    let body = document.getElementsByTagName('body')[0]
    console.log(body)

    body.innerHTML = `
        <div>
            </br>
            </br>
            </br>
            ${href}
            </br>
            </br>
            </br>
            <button onClick="redirectLocation()">Redirect with assign</button>
            <button onClick="redirectHref()">Redirect with href</button>
            <button onClick="redirectOpen()">Redirect with open</button>
        </div>
    `
}

redirectOpen()

// init()
